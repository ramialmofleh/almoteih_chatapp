export default {
  nachrichten: [
    {
      name: 'Rami Almofleh',
      id: 1,
      bild: 'account_circle',
      nachrichten:[
        {
          nachricht: ['Hey Rami'],
          date: '20.01.2020',
          time: '12:31',
          sender: 0
        },
        {
          nachricht: ['Hi', 'was los?'],
          date: '20.01.2020',
          time: '12:34',
          sender: 1
        }
      ]
    },
    {
      name: 'Julian Theil',
      id: 2,
      nachrichten: [],
      bild: 'supervised_user_circle'
    },
    {
      name: 'Ghadeer Ho',
      id: 3,
      nachrichten: [],
      bild: 'favorite'
    },
    {
      name: 'Michael',
      id: 4,
      nachrichten: [],
      bild: 'group_work'
    },
    {
      name: 'Christiane B',
      id: 5,
      nachrichten: [],
      bild: 'copyright'
    }
  ]
}
