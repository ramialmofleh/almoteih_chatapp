export function nachrichtSpeichern (state, benutzer) {
  const idx = state.nachrichten.findIndex((e) => {
    return e.id === benutzer.id;
  });

  const letzteNachricht = state.nachrichten[idx].nachrichten[state.nachrichten[idx].nachrichten.length - 1];
  if(letzteNachricht != null && letzteNachricht.time === benutzer.nachricht.time) {
    state.nachrichten[idx].nachrichten[state.nachrichten[idx].nachrichten.length - 1].nachricht.push(benutzer.nachricht.nachricht[0]);
  } else {
    state.nachrichten[idx].nachrichten.push(benutzer.nachricht);
  }
}
