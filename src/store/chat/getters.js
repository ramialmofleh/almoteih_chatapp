export function getNachrichten (state) {
  return state.nachrichten;
}

export function getBenutzer (state) {
  return (id) => {
    const idx = state.nachrichten.findIndex((e) => {
      return e.id === Number(id);
    });

    return state.nachrichten[idx];
  }
}
