
const routes = [
  {
    path: '/',
    component: () => import('layouts/RegisterAnmeldeLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Anmelden.vue') }
    ]
  },
  {
    path: '/anmelden',
    component: () => import('layouts/RegisterAnmeldeLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Anmelden.vue') }
    ]
  },
  {
    path: '/register',
    component: () => import('layouts/RegisterAnmeldeLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Register.vue') }
    ]
  },
  {
    path: '/home',
    component: () => import('layouts/FirstLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Home.vue') }
    ]
  },
  {
    path: '/freundehinzufuegen',
    component: () => import('layouts/FirstLayout.vue'),
    children: [
      { path: '', component: () => import('pages/FreundeHinzufuegen.vue') }
    ]
  },
  {
    path: '/einstellungen',
    component: () => import('layouts/FirstLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Einstellungen.vue') }
    ]
  },
  {
    path: '/sprache',
    component: () => import('layouts/SecondLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Sprache.vue') }
    ]
  },
  {
    path: '/meinprofil',
    component: () => import('layouts/FirstLayout.vue'),
    children: [
      { path: '', component: () => import('pages/MeinProfil.vue') }
    ]
  },
  {
    path: '/chat',
    component: () => import('layouts/SecondLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Chat.vue') }
    ]
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
